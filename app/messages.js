const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = './messages';

router.get('/', (req, res) => {
    let messages = [];
    fs.readdir(path, (err, files) => {
        files.slice(-5).forEach(file => {
            const message = fs.readFileSync(`./messages/${file}`);
            messages.push(JSON.parse(message));
        });
        res.send(messages);
    })
});

router.post('/', (req, res) => {
    const datetime = new Date().toISOString();
    const reqBodyKey = Object.keys(req.body);
    const reqBodyValue = Object.values(req.body).join();

    res.send({[reqBodyKey]: reqBodyValue, datetime: datetime});
    fs.writeFile(`./messages/${datetime}.txt`, JSON.stringify(req.body), err => {
        if (err) {
            console.error(err);
        } else {
            console.log('File was saved');
        }
    });
});

module.exports = router;